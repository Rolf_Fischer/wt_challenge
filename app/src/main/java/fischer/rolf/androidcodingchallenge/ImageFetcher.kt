package fischer.rolf.androidcodingchallenge

import android.graphics.BitmapFactory
import android.util.Log
import android.widget.ImageView
import okhttp3.*
import java.io.BufferedInputStream
import java.io.IOException

private const val TAG = "ImageFetcher"

/**
 * Fetches images from URLs, placing them in ImageViews.
 */
class ImageFetcher {

    private var client = OkHttpClient.Builder().connectionSpecs(arrayListOf(ConnectionSpec.CLEARTEXT)).build()
    private val calls = ArrayList<Call>()

    /**
     * Fetch an image from the specified, placing it in the specified ImageView upon success.
     * Fails silently.
     * @param imageURL URL of image to fetch
     * @param imageView ImageView to display fetched image
     */
    fun fetchImage(imageURL: String?, imageView: ImageView) {
        if (imageURL == null)
            return

        // start an asynchronous request for the image
        val request = Request.Builder().url(imageURL).get().build()
        val call = client.newCall(request)
        calls.add(call)
        call.enqueue(object : Callback {
            @Throws(IOException::class)
            override fun onResponse(call: Call, response: Response) {
                try {
                    // check some pre-conditions
                    if (!response.isSuccessful)
                        throw RuntimeException("Unsuccessful response")
                    if (response.code != 200)
                        throw RuntimeException("Response code = '" + response.code + "'")
                    if (response.body == null)
                        throw RuntimeException("Response body empty")

                    // decode body into a bitmap
                    val bufferedInputStream = BufferedInputStream(response.body!!.byteStream())
                    val bitmap = BitmapFactory.decodeStream(bufferedInputStream)

                    // assign to specified ImageView using UI thread
                    imageView.post {
                        imageView.setImageBitmap(bitmap)
                    }
                } catch (e: Exception) {
                    Log.e(TAG, "Error getting image.", e)
                }
            }

            override fun onFailure(call: Call, e: IOException) {
                Log.e(TAG, "Error getting image.", e)
            }
        })
    }

    /**
     * Cancel any and all pending calls.
     */
    fun cancelAll() {
        for (call in calls)
            call.cancel()
        calls.clear()
    }
}