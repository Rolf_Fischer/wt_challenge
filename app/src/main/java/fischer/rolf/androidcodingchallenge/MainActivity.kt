package fischer.rolf.androidcodingchallenge

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.ListView
import android.widget.TextView
import kotlin.collections.ArrayList

/**
 * Our main activity that displays a list of books fetched from the web.
 */
class MainActivity : AppCompatActivity(), BookFetcher.Handler {

    private var listViewBooks: ListView? = null
    private var textViewMessage: TextView? = null
    private val bookFetcher = BookFetcher()
    private var booksAdapter: BooksAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // get needed UI controls
        listViewBooks = findViewById(R.id.list_view_books)
        textViewMessage = findViewById(R.id.text_view_message)

        // fetch the list of books, asynchronously
        bookFetcher.getBooks(this)
    }

    override fun onDestroy() {
        // make sure to cancel book fetch if it's still pending
        bookFetcher.cancel()

        // inform adapter we are being destroyed
        booksAdapter?.onDestroy()

        super.onDestroy()
    }

    override fun onBooksFetched(books: ArrayList<Book>) {
        // post to UI thread
        listViewBooks?.post {
            // create our book adapter and assign it to our book ListView
            textViewMessage?.visibility = View.GONE
            booksAdapter = BooksAdapter(this, books)
            listViewBooks?.adapter = booksAdapter
        }
    }

    override fun onBookFetchFailed() {
        // post to UI thread
        listViewBooks?.post {
            // inform the user we failed
            textViewMessage?.setText(R.string.error_fetching_books)
        }
    }
}
