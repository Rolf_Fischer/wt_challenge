package fischer.rolf.androidcodingchallenge

/**
 * Encapsulates a book.
 * @param title book title
 * @param author book author
 * @param imageURL book thumbnail image URL
 */
class Book(var title: String?, var author: String?, var imageURL: String?)