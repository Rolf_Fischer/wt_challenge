package fischer.rolf.androidcodingchallenge

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.ImageView
import android.widget.TextView

/**
 * Adapter for showing a list of books.
 * @param context context
 * @param books list of books to display
 */
class BooksAdapter(context: Context, books: ArrayList<Book>) : ArrayAdapter<Book>(context, 0, books) {

    private val imageFetcher = ImageFetcher()

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {

        // inflate our view, if necessary
        var view = convertView
        if (view == null)
            view = LayoutInflater.from(context).inflate(R.layout.item_book, parent, false)

        // at this point the view can't be null
        view!!

        // get the book info views
        val ivThumb: ImageView? = view.findViewById(R.id.image_view_thumbnail)
        val tvTitle: TextView? = view.findViewById(R.id.text_view_title)
        val tvAuthorLabel: TextView? = view.findViewById(R.id.text_view_author_label)
        val tvAuthor: TextView? = view.findViewById(R.id.text_view_author)

        // fill in the UI with the book's info
        val book = getItem(position)
        book?.let {

            // clear any thumb that might be there since views are reused
            ivThumb?.setImageBitmap(null)
            if (ivThumb != null && book.imageURL != null)
                imageFetcher.fetchImage(book.imageURL, ivThumb)

            tvTitle?.setText(if (book.title == null) "" else book.title)
            if (book.author == null) {
                tvAuthorLabel?.visibility = View.GONE
                tvAuthor?.visibility = View.GONE
            } else {
                tvAuthorLabel?.visibility = View.VISIBLE
                tvAuthor?.visibility = View.VISIBLE
                tvAuthor?.setText(book.author)
            }
        }

        return view
    }

    /**
     * Clean up since we are being destroyed.
     */
    fun onDestroy() {
        imageFetcher.cancelAll()
    }

}