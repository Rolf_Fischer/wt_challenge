package fischer.rolf.androidcodingchallenge

import android.util.Log
import okhttp3.*
import org.json.JSONArray
import java.io.IOException
import java.lang.RuntimeException

private const val TAG = "BookWebService"
private const val BOOKS_URL = "https://de-coding-test.s3.amazonaws.com/books.json"

private const val JSON_TITLE = "title"
private const val JSON_AUTHOR = "author"
private const val JSON_IMAGE_URL = "imageURL"

/**
 * Helper class used to fetch a list of books from a web server.
 */
class BookFetcher {

    private val client = OkHttpClient.Builder().build()
    private val request = Request.Builder().url(BOOKS_URL).get().build()
    private var call: Call? = null

    /**
     * Implement the handler interface to be informed when books have been
     * fetched successful or unsuccessfully.
     */
    interface Handler {
        /**
         * This is invoked when the book fetch has succeeded.
         * @param books list of books that were fetched
         */
        fun onBooksFetched(books: ArrayList<Book>)

        /**
         * This is invoked when the book fetch has failed.
         */
        fun onBookFetchFailed()
    }

    /**
     * Asynchronously get books from a web server.
     * @param booksHandler handler we will asynchronously inform of success or failure
     */
    fun getBooks(booksHandler: Handler) {
        // cancel any pending calls
        cancel()

        // perform an asynchronous call to get the books
        call = client.newCall(request)
        call?.enqueue(object : Callback {
            @Throws(IOException::class)
            override fun onResponse(call: Call, response: Response) {
                try {
                    // check some pre-conditions
                    if (!response.isSuccessful)
                        throw RuntimeException("Unsuccessful response")
                    if (response.code != 200)
                        throw RuntimeException("Response code = '" + response.code + "'")
                    if (response.body == null)
                        throw RuntimeException("Response body empty")

                    // parse our list of books from the JSON
                    val books = ArrayList<Book>()
                    val jsonBooks = JSONArray(response.body!!.string())
                    for (i in 0 until jsonBooks.length()) {
                        val jsonBook = jsonBooks.optJSONObject(i) ?: continue

                        var title: String? = null
                        var author: String? = null
                        var imageURL: String? = null

                        if (jsonBook.has(JSON_TITLE))
                            title = jsonBook.getString(JSON_TITLE)
                        if (jsonBook.has(JSON_AUTHOR))
                            author = jsonBook.getString(JSON_AUTHOR)
                        if (jsonBook.has(JSON_IMAGE_URL))
                            imageURL = jsonBook.getString(JSON_IMAGE_URL)

                        val book = Book(title, author, imageURL)
                        books.add(book)
                    }

                    // give the books to the handler
                    booksHandler.onBooksFetched(books)
                } catch (e: Exception) {
                    Log.e(TAG, "Error getting books.", e)

                    // inform handler we failed
                    booksHandler.onBookFetchFailed()
                }
            }

            override fun onFailure(call: Call, e: IOException) {
                Log.e(TAG, "Error getting books.", e)

                // inform handler we failed
                booksHandler.onBookFetchFailed()
            }
        })
    }

    /**
     * Cancel any pending book fetch.
     */
    fun cancel() {
        if (call != null) {
            call?.cancel()
            call = null
        }
    }
}